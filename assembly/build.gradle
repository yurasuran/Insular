apply plugin: 'com.android.application'

// Try reading secrets from file
def secretsPropertiesFile = rootProject.file("secrets.properties")
def secretProperties = new Properties()

if (secretsPropertiesFile.exists()) {
    secretProperties.load(new FileInputStream(secretsPropertiesFile))
}
// Otherwise read from environment variables, this happens in CI
else {
    secretProperties.setProperty("signing_keystore_password", "${System.getenv('KEYSTORE_PASSWORD')}")
    secretProperties.setProperty("signing_key_password", "${System.getenv('KEY_PASSWORD')}")
    secretProperties.setProperty("signing_key_alias", "${System.getenv('KEY_ALIAS')}")
}

android {
    compileSdkVersion this.compileSdkVersion

    defaultConfig {
        applicationId "com.oasisfeng.island"
        minSdkVersion this.minSdkVersion
        targetSdkVersion 29     // Private APIs are accessed by module "open" and "fileprovider" (via indirectly loaded remote class)
        resConfigs "en", "zh", "zh-rTW"
        testInstrumentationRunner 'androidx.test.runner.AndroidJUnitRunner'
        missingDimensionStrategy 'compat', 'platformDeprecated'  // setup-wizard has a "compat" dimension that Insular doesn't
        versionCode 51507
        // Manually bump the semver version part of the string as necessary
        versionName '5.1.2-a3fe6196'
    }

    buildFeatures.dataBinding true

    signingConfigs {
        release {
            // You need to specify either an absolute path or include the
            // keystore file in the same directory as the build.gradle file.
            storeFile file("../android-signing-keystore.jks")
            storePassword "${secretProperties['signing_keystore_password']}"
            keyAlias "${secretProperties['signing_key_alias']}"
            keyPassword "${secretProperties['signing_key_password']}"
        }
    }

    buildTypes {
        debug {
        }
        release {
            minifyEnabled true
            proguardFiles getDefaultProguardFile('proguard-android-optimize.txt'), 'proguard-rules.pro'
            signingConfig signingConfigs.release
        }
    }

    flavorDimensions "packaging", "version"

    productFlavors {
        complete {
            dimension "packaging"
        }
        engine {    // Use the same application ID to retain the profile / device ownership between "engine" and "full" build.
            dimension "packaging"
            matchingFallbacks = ['full']
        }
        mobile {
            applicationIdSuffix ".mobile"
            dimension "packaging"
            matchingFallbacks = ['full']
        }
        fileprovider {
            applicationIdSuffix ".fileprovider"
            dimension "packaging"
            matchingFallbacks = ['full']
        }

        playstore {
            dimension "version"
        }
        fdroid {
            dimension "version"
            applicationIdSuffix = ".fdroid"
        }
    }

    android.applicationVariants.all { variant ->
        variant.mergeResources.doFirst {
            variant.sourceSets.each { sourceSet ->
                sourceSet.res.srcDirs = sourceSet.res.srcDirs.collect { dir ->
                    def relDir = relativePath(dir)
                    delete '**/analytics_tracker.xml'
                    copy {
                        from(dir)
                        include '**/*.xml'
                        filteringCharset = 'UTF-8'
                        filter {
                            line -> line
                                    .replace('Island', 'Insular')
                        }
                        into("${buildDir}/tmp/${variant.dirName}/${relDir}")
                    }
                    copy {
                        from(dir)
                        exclude '**/*.xml'
                        into("${buildDir}/tmp/${variant.dirName}/${relDir}")
                    }
                    return "${buildDir}/tmp/${variant.dirName}/${relDir}"
                }
            }
        }
    }

    compileOptions {
        sourceCompatibility JavaVersion.VERSION_1_8
        targetCompatibility JavaVersion.VERSION_1_8
    }

    lintOptions {
        check 'NewApi'
        abortOnError true
        htmlReport false
        xmlReport false
        textReport true
        textOutput "stdout"
    }

    repositories.flatDir {
        dirs '../app/libs'
    }
}

dependencies {
    // Complete
    completeImplementation project(':engine')
    completeImplementation project(':mobile')
    completeImplementation project(':fileprovider')
    completeImplementation project(':installer')
    completeImplementation project(':watcher')
    completeImplementation project(':open')
    // Engine only
    engineImplementation project(':engine')
    // Mobile only
    mobileImplementation project(':mobile')
    // File Provider only
    fileproviderImplementation project(':fileprovider')
}
